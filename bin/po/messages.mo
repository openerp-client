��    G     T
  �  �      h  �   i  �   "            8   $  #   ]  (   �  (   �     �     �     �     �       
     
      
   +  
   6  
   A  
   L  
   W  
   b  
   m  
   x     �     �     �     �     �  �   �     �  J   �     �     
   '   #   �   K      2!  #   3"     W"  %   t"     �"     �"     �"     �"     �"     #      #     0#     J#     d#  #   �#      �#     �#     �#     �#     $     2$     G$     Z$     o$     �$     �$     �$     �$  ?   �$  t   %  0   �%     �%     �%     �%     �%  "   �%     &     0&  
   C&     N&     g&     s&     z&     �&     �&  J   �&     �&  �    '  w   �'     (     ,(  	   8(     B(     T(     p(     y(     �(     �(     �(     �(     �(     �(     �(     �(     )     )  	   1)  	   ;)     E)     T)     a)     o)     )     �)     �)     �)     �)     �)     �)     �)     �)  
   *  	   *      *     .*     D*     Z*     j*     m*     p*     �*  	   �*     �*     �*     �*     �*     �*     �*     �*     +     +     !+  
   .+     9+     ?+     Z+     j+  	   z+     �+     �+  "   �+     �+     �+     �+     �+     	,     ,     ,     ,     %,     .,  	   4,     >,     B,     U,     p,     ~,     �,     �,     �,     �,     �,  $   �,     �,     �,     �,     
-     $-     =-     E-     [-     k-  	   |-     �-     �-     �-  /   �-     �-  	   �-     �-     .     .     -.     6.     C.     \.     l.     y.     �.     �.     �.     �.     �.     �.  	   �.     /     /     /     -/     6/  	   ;/     E/  	   [/     e/  
   ~/     �/     �/     �/     �/     �/     �/     �/     �/  	   0     0     0     .0     >0     R0     c0     k0     �0  A   �0     �0     �0  '   �0  �   $1     �1     �1     2     #2      ;2     \2     v2     �2     �2     �2     �2     �2     �2     �2     3     3     03     J3     \3     v3     �3     �3     �3     �3     �3     �3     �3  !   4     14     @4     F4     J4     W4     ]4     o4  
   x4  %   �4     �4     �4     �4  j   �4  
   =5     H5  	   X5     b5     g5     x5     �5     �5     �5     �5     �5     �5     �5  
   �5     �5     �5     6     6     "6     (6     46     =6     C6     L6     Q6     _6     m6     v6     |6     �6     �6     �6     �6     �6     �6     �6     �6     �6     �6     �6  
   
7  
   7      7  	   '7     17  	   77     A7     G7     Y7     g7     v7  
   �7     �7  A  �7  �   �8    �9     �:     �:  S   �:  /   ;  /   J;  1   z;     �;     �;     �;     �;     �;  
   �;  
   �;  
   <  
   <  
   <  
   %<  
   0<  
   ;<  
   F<  
   Q<     \<     d<     k<     r<     �<    �<     �=  R   �=     >  '   $>  4   L>     �>    �?  )   �@  +   �@  9   �@     .A     @A     ZA     tA     �A     �A     �A  !   �A  %   �A     B  +   1B  (   ]B  &   �B     �B     �B     �B     C     C     1C     JC     _C     xC     �C  $   �C  M   �C  �   D  E   �D     �D     �D     �D      E  )   ,E     VE     rE     �E     �E     �E  
   �E     �E     �E     �E  o   F  (   rF  �   �F     ?G     �G     �G     �G     �G      H     H     +H     4H  
   LH     WH     wH     �H  $   �H     �H     �H     �H     I     4I     GI     YI     hI     uI     �I     �I  	   �I     �I     �I     �I     J  %   J     4J     KJ  	   `J  	   jJ     tJ  !   �J     �J     �J     �J     �J     �J     �J     
K     K     $K  
   -K     8K     NK     ZK  "   yK     �K     �K     �K     �K     �K     �K     L     ,L  	   FL     PL     cL  2   vL  	   �L     �L     �L     �L     �L      M     	M     M     M     M     (M     9M     AM  &   ]M     �M     �M     �M     �M     �M     �M     �M  $   �M     N     (N     9N  $   QN     vN  	   �N     �N     �N     �N     �N     �N      O     O  ;   2O     nO     uO     �O     �O     �O     �O     �O     �O     P     (P     ;P     RP     kP     ~P  "   �P     �P     �P  
   �P     �P  	   Q     Q     5Q     AQ     HQ     ^Q     {Q     �Q     �Q     �Q     �Q     �Q     	R  	   R     R      2R     SR  
   sR     ~R     �R     �R     �R     �R  
   �R  #   �R      S  O   :S     �S     �S  -   �S  �   �S     �T     �T     �T      U      U     9U     WU     kU     }U     �U     �U     �U     �U     �U     �U     V     V     ?V     SV     sV     �V     �V     �V     �V     �V     �V     W  +   $W     PW     _W  	   eW     oW     �W     �W  	   �W     �W  +   �W     �W     X     &X  ~   *X     �X     �X     �X     �X      �X     Y     Y     Y  
   +Y  	   6Y     @Y  1   NY  
   �Y  
   �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y      Z     Z     Z     Z     4Z     OZ     UZ     ]Z     fZ     oZ     ~Z  	   �Z     �Z  
   �Z     �Z     �Z     �Z     �Z     [     #[     /[     @[  	   H[     R[     [[     m[     z[     �[     �[     �[     �[     �[     �         k   b   �         P       �   K               �   �       A       �       �   c   B                      J       �   _   �               �   3   M   0   p   
   �      �   �   �   >  -  )       �   A          �       �   7           �       ^     E   �           �   4   ;                       1      �       `            j   (  I   �   3  �   	       F   d   �   H   /  �   �   �   �           l   �       �   �   �       /       �   �   '  &   �       L   �         y   �   �   <   >               6         �       �          f   �   2  )  �       !    9                �   x           8   
          �   �   �   �   .  �       {       �       Q   �   �               ]   R       D   F  ,           (                    +  �   �   �   ?              �                    �   �       9      �           �        !   �   �      �   �   "   =   +       �   Z      E  N   ~   �   =  �   �   :      U   s   �   "  W   �   C  �     n           �   T   m   �         Y       �       �   #  V       �   �          $   g   O     2   �   	           �       �   \       |   }   G  0  �     �   �   �   @  :       �       o   ?  �       ;   6  �   1       �   w   �   �   �   5         �       @       �       �       D  C   [   %     �      �   �   8          r   &  �   ,       �       �   �       �   �      �         *      �   �       $      �   X       v       �         7  B   �   �       t       �   5   �   e   %  #   �   �       �          �       i   �   u   a   S   �           q       �         �   *   z          G   �   '        4  .       �   h   �   �   �     �          �   �       <      -   �        
(c) 2003-TODAY - Tiny sprl
Tiny ERP is a product of Tiny sprl:

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgium

Tel : (+32)81.81.37.00
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - GTK Client - v%s

Tiny ERP is an Open Source ERP+CRM
for small to medium businesses.

The whole source code is distributed under
the terms of the GNU Public Licence.

(c) 2003-TODAY, Tiny sprl

More Info on www.TinyERP.com ! 
Yes
No # Employees: (Get Tiny ERP announces, docs and new releases by email) (must not contain any special char) (must not contain any special character) (use 'admin', if you did not changed it) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shist> + <Tab> <Tab> <b>
Write concurrency warning:
</b>
This document has been modified while you were editing it.
  Choose:
   - "Cancel" to cancel saving.
   - "Compare" to see the modified version.
   - "Write anyway" to save your current version.
 <b><u>Actions</u></b> <b>About Tiny ERP</b>
<i>The most advanced Open Source ERP &amp; CRM !</i> <b>All fields</b> <b>Backup a database</b> <b>Change your super admin password</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Complete this form to submit your bug and/or send a support request.</b>
<i>
Your request will be sent to the Tiny ERP team and we will reply shortly.
Note that we may not reply if you do not have a support contract with Tiny or an official partner.</i> <b>Connect to a Tiny ERP server</b> <b>Create a new database</b> <b>Database created successfully!</b> <b>Error 404</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Keep Informed</b> <b>Operation in progress</b> <b>Opti_ons</b> <b>Predefined exports</b> <b>Restore a database</b> <b>Shortcuts for TinyERP</b> <b>Shortcuts in relation fields</b> <b>Shortcuts in text entries</b> <b>Support contract id:</b> <b>Tiny ERP Survey</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <b>Your Company:</b> <b>Your Email:</b> <b>Your company:</b> <b>Your email:</b> <b>Your interrest:</b> <b>Your name:</b> <b>_Support Request</b> <b>support contract id</b> <i>Click on the Support Request tab if you need more help !</i> <i>Please fill in the following form in order to help us to improve Tiny ERP and better target new developments.</i> <i>When editing a resource in a popup window</i> AL_L Action Add _field names Add a new line/field Add an attachment to this resource Administrator Password Application Error! Attachment Auto-Complete text field Auto-Detect Bottom CSV Parameters Change Change password Check this box if you want demo data to be installed on your new database. Choose a database... Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database that will be created. The name must not contain any special character. Exemple: 'terp'. Clear the field Client Mode Close Tab Close this window Close window without saving Company: Compare Concurrency exception Connect Copy selected text Copy this resource Country: Create a new database Create a new entry Create a new resource Cut selected text Database creation Database: Databases Date de début De_scription Default Theme Default _value: Default language: Delete Delete this resource Description: Dro_p database E-Mail: Edit / Save this resource Edit this entry Edition Widgets Emergency: Encoding: Error details Explain what you did: Explain your problem: Expor_t data... F1 F2 Field Separater: Field _Name: Filename: Filter: Find Find / Search Find a resource For_m Go to next matched resource Go to previous matched search Go to resource ID Hello World Hello World! Horizontal Hour: How did you hear about us: I_mport data... Import from CSV Industry: Keyboard Shortcuts Keyboard shortcuts Launch actions about this resource Left Lines to Skip: Load demonstration data: Main Shortcuts Menu Minute: Mode PDA N_ONE N_othing Ne_xt Ne_xt Tip New New database name: New password confirmation: New password: Next Next Tab Next editable widget Next record No Normal Not Urgent
Medium
Urgent
Very Urgent Old password: Open Source: Open current field Open in Excel
Save as CSV Open the calendar widget Open... Operation in progress Other comments: Others Comments: Password: Paste selected text Phone Number: Phone number: Please wait,
this operation may take a while... Port: Pre_vious Pre_vious Tip Previe_w before print Previe_w in editor Previous Previous Tab Previous editable widget Previous record Previous tab Print documents Protocol connection: Read my Requests Reloa_d / Undo Remove selected entry Remove this entry Repeat latest _action Requests: Restore a database Right Right Toolbar S_earch: Save Save List Save and Close window Save text Search / Open a resource Search ID: Search with this name Send Support Request Send a new request Server Server: Set Default Set to _Default Set to default value Shortcuts State: Super admin password: Support Request Switch to list/form Switch view mode System: Tabs default orientation Tabs default position Tell us why you try Tiny ERP and what are your current softwares: Text Delimiter: Text _and Icons This Page does not exist !
Please retry This is the password of the user that have the rights to administer databases. This is not a Tiny ERP user, just a super administrator. If you did not changed it, the password is 'admin' after installation. Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - License Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Selection Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP Message Tiny ERP Survey Tiny ERP Theme Tiny ERP license Tiny ERP server: Tiny ERP, Field Preference target Tip of the Day Titre Top User _Manual User: Value _Preference Vertical View _logs We plan to offer services on Tiny ERP We plan to use Tiny ERP Write anyway Yes You can connect to the new database using one of the following account:

    Administrator: admin / admin  Your Role: Your selection: _About... _Add _Backup database _Connect... _Contact _Contextual Help _Delete _Details _Disconnect _Display a new tip next time? _Domain: _Duplicate _Execute a plugin _File _Forms _Go to resource ID... _Help _Icons only _License _Menu _Menubar _New _New Home Tab _New database _Nothing _Open _Options _Plugins _Preferences _Preview in PDF _Print _Read my requests _Remove _Restore database _Save _Save options _Select _Send a request _Shortcuts _Text only _Theme _Tiny ERP _Tips _Unselect _User _Waiting Requests _only for you for _all users http://localhost:8069 on _field: test Project-Id-Version: Tiny ERP V4.1.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-08-21 13:38+0200
PO-Revision-Date: 2006-11-06 00:15+0100
Last-Translator: Cédric Krier <ced@tinyerp.com>
Language-Team: FR <i18n-info@tinyerp.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
(c) 2003-aujourd'hui - Tiny sprl
Tiny ERP est un produit de Tiny sprl :

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgium

Tel : (+32)81.81.37.00
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - Client GTK - v%s

Tiny ERP est un ERP+CRM Open Source pour
petites et moyennes entreprises.

Le code source dans son intégralité est distribué sous
les termes de la GNU Public Licence.

(c) 2003-aujourd'hui, Tiny sprl

Plus d'infos sur www.TinyERP.com ! 
Oui
Non # d'employés : (Recevez les annonces Tiny ERP, les documentations et nouvelles versions par email) (ne doit pas contenir de caractères spéciaux) (ne doit pas contenir de caractères spéciaux) (Utilisez 'admin', si vous ne l'avez pas changé) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shift> + <Tab> <Tab> <b>
Avertissement d'écriture concurrencielle :
</b>
Ce document a été modifié pendant qu'vous l'éditiez.
  Choisissez :
   - "Annuler" pour annuler la sauvegarde.
   - "Comparer" pour voir la version modifiée.
   - "Écrire de toute façon" pour sauver votre version courante.
 <b><u>Actions</u></b> <b>A propos de Tiny ERP</b>
<i>Le plus avancé des ERP &amp; CRM Open Source !</i> <b>Tous les champs</b> <b>Sauvegarder une base de données</b> <b>Modifier le mot de passe super administrateur</b> <b>Complétez ce formulaire pour soumettre un bug et/ou une demande de support.</b>
<i>
Votre demande sera envoyée à l'équipe de support de Tiny ERP.
Veuillez noter que nous pourrions ne pas répondre si votre Société n'est paspartenaire TinyERP.
</i> <b>Complétez ce formulaire pour soumettre un bug et/ou une demande de support.</b>
<i>
Votre demande sera envoyée à l'équipe de support de Tiny ERP.
Veuillez noter que nous pouvrions ne pas répondre si vous n' avez pas de contrat de support avec Tiny ou partenaire officiel.</i> <b>Se connecter a un serveur Tiny ERP</b> <b>Créer une nouvelle base de données</b> <b>La base de données a été créée avec succès !</b> <b>Erreur 404</b> <b>Code de l'erreur :</b> <b>Champs à exporter</b> <b>Champs à importer</b> <b>Restez Informé</b> <b>Operation en cours</b> <b>Opti_ons</b> <b>Exportations prédéfinies</b> <b>Restaurer une base de données</b> <b>Raccourcis TinyERP</b> <b>Raccourcis dans les champs relations</b> <b>Raccourcis dans les champs textes</b> <b>Numéro de contrat de support :</b> <b>Sondage Tiny ERP</b> <b>Valeur applicable pour :</b> <b>Valeur applicable si :</b> <b>Votre société :</b> <b>Votre email :</b> <b>Votre société :</b> <b>Votre email :</b> <b>Votre intérêt :</b> <b>Votre nom :</b> <b>Demande de _support</b> <b>Numéro de contrat de support</b> <i>Cliquer sur l'onglet Demande de support si vous avez besoin d'aide ! !</i> <i>Merci de compléter le formulaire suivant afin de nous aider à améliorer Tiny ERP et à cibler les développements futurs.</i> <i>Lors de la modification d'une ressource dans un fenêtre popup</i> _TOUT Action Ajouter les noms de _champ Ajouter une nouvelle ligne/champ Ajouter un attachement à cette ressource Mot de passe administrateur Erreur de l'application ! Attachement Auto-completion du champ text Auto-Détection En dessous Paramètres CSV Changer Modifier le mot de passe Cochez cette case si vous désirez installer des données de démomonstration dans la nouvelle base de données Veuillez choisir une base de données... Choisissez la langue par défaut qui sera installée pour cette base de données. Vous pourrez installer d'autres langues par la suite via le menu d'administration Choisissez le nom de la base de données qui sera créée. Le nom ne doit contenir aucun caractère spécial. Exemple : 'terp'. Effacer le champ Mode Client Fermer onglet Fermer cette fenêtre Fermer la fenêtre sans sauver Société : Comparer Exception de concurence _Connecter Copier l'entrée sélectionnée Copier cette ressource Pays : Créer une nouvelle base de données Créer une nouvelle entrée Créer une nouvelle ressource Couper l'entrée sélectionnée Creation de la base de données Base de données : Bases de données Date de début De_scription _Thème par défaut _Valeur par défaut : Langue par défaut : Supprimer Supprimer cette ressource Description : Su_pprimer la base de données E-Mail : Éditer / Enregistrer cette ressource Modifier cette entrée Édition des widgets Urgence : Encodage: Détails de l'erreur Expliquez ce que vous avez fait : Expliquez votre problème : E_xporter données... F1 F2 Séparateur de champ : _Nom du champ : Nom du fichier : Filtre : Chercher Rechercher Trouver une ressource For_mulaire Aller à la ressource suivante Aller à la ressource précédente Aller à la ressource ID Bonjour tout le monde! Bonjour tout le monde! Horizontale Heure : Comment nous connaissez-vous : I_mporter données... Importer d'un fichier CSV Secteur : Raccourcis clavier Raccourcis clavier Exécuter les actions à propos de cette ressource À gauche Lignes à ignorer : Charger les données démos : Raccourcis principaux Menu Minute : Mode PDA _AUCUN _Rien S_uivant Astuce _suivante Nouveau Nouvelle base de données : Confirmation du nouveau mot de passe : Nouveau mot de passe : Suivant Onglet suivant Prochain widget editable Enregistrement suivant Non Normal Non Urgent
Moyen
Urgent
Très Urgent Ancien mot de passe : Logiciel Libre : Ouvrir le champ courant Ouvrir dans Excel
Enregistrer en CSV Ouvrir le calendrier Ouvrir... Operation en cours Autres commentaires : Autres remarques : Mot de passe : Coller l'entrée sélectionnée Numéro de téléphone : Numéro de téléphone : Patientez,
cette opération peut durer quelques instants... Port : _Précédente Astuce _précédente _Aperçu avant impression Pré_visualiser dans l'éditeur Précédent Onglet précédent Précédent widget editable Enregistrement précédent Onglet précédent Imprimer les documents Protocol de connection : Lire mes requêtes Rec_harger / Annuler Supprimer l'entrée sélectionnée Supprimer cette entrée Refaire _dernière action Requêtes: Restaurer une base de données À droite Barre d'outils de droite Ch_ercher : Sauver _Enregistrer la liste Sauver et fermer la fenêtre Sauver le texte Chercher/Ouvrir des ressources Chercher ID : Chercher avec ce nom Envoyer la demande de support Envoyer une nouvelle requête Serveur Serveur : Définir comme défaut Mettre à la valeur par _défaut Mettre à la valeur par défaut Raccourcis État : Mot de passe super admin : Demande de support Basculer vers liste/formulaire Changer de mode de vue Système : Orientation des onglets par défaut Position des onglets par défaut Dites nous pourquoi vous essayez Tiny ERP et quels sont vos logiciels courant : Délimiteur de texte : Texte _et icônes Cette page n'existe pas !
Veuillez réessayer Ceci est le mot de passe de l'utilisateur qui a les droits de gestion des bases de données. Il n'est pas un utilisateur Tiny mais un super administrateur. Si vous ne le changez pas, le mot de passe après l'installation est 'admin'. Tiny ERP Tiny ERP - À propos Tiny ERP - Attachement Tiny ERP - Confirmation Tiny ERP - Sélection date/heure Tiny ERP - Sélection de date Tiny ERP - Dialogue Tiny ERP - Erreur Tiny ERP - Exporter en CSV Tiny ERP - Filtre Tiny ERP - Formulaires Tiny ERP - Licence Tiny ERP - Lien Tiny ERP - Liste Tiny ERP - Login Tiny ERP - Préférences Tiny ERP - Edition de ressource Tiny ERP - Chercher Tiny ERP - Widgets de recherche Tiny ERP - Sélection Tiny ERP - Arbre des ressources Tiny ERP - widgets formulaires Message de Tiny ERP Sondage Tiny ERP Thème Tiny ERP Licence de Tiny ERP Tiny ERP serveur : Tiny ERP, cible de la préférence du champ Astuce du jour Titre Au dessus _Manuel utilisateur Utilisateur : _Préférence de valeur Verticale Voir historique Nous pensons offrir du service sur Tiny ERP Nous pensons utiliser Tiny ERP Écrire de toute façon Oui Vous pouvez vous connecter à la nouvelle base de données en utilisant l'accès suivant :

    Administrateur: admin / admin  Votre rôle : Votre sélection : A _propos... _Ajouter _Sauvegarder la base de données _Connecter... _Contact Aide _contextuelle _Supprimer _Détails _Déconnecter _Afficher une nouvelle astuce la prochaine fois ? _Domaine : _Dupliquer _Exécuter un plugin _Fichier For_mulaires _Aller à la ressource ID... _Aide _Icônes seulement _Licence _Menu Barre de _menu _Nouveau _Nouvel onglet Racine _Nouvelle base de données _Rien _Ouvrir _Options _Plugins _Préférences _Prévisualiser en PDF Im_primer _Lire mes requêtes _Supprimer _Restaurer la base de données _Sauver _Enregistrer les options _Sélectionner _Envoyer une requête _Raccourcis _Texte seulement _Thème _Tiny ERP _Astuces _Désélectionner _Utilisateur _Requêtes en attente _seulement pour vous pour _tous les utilisateurs http://localhost:8069 sur le _champ : test 