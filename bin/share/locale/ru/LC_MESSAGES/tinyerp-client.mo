��    �      4             L     T     V     l  �   ~      e	     �	     �	     �	     �	     �	     �	     
  ?   *
     j
     o
     v
     �
     �
     �
     �
     �
     �
     �
               +  	   8     B     H     d     �  "   �     �     �     �  	   �     �     �     �  $   �            	   5     ?  	   E     O     e     x     �     �     �     �     �  	   �     �  
   �     �          )     <     D     T  	   i     s  '   �     �     �     �     �      �          .     @     Q     j     |     �     �     �     �     �     �               5     M  !   ^     �     �     �     �  
   �     �     �  	   �     �     �     �     �     �                    %     7     =     C     O     X     ]     f     l     u     ~     �     �     �     �     �     �     �     �     �  	   �     �     �            
   -     8     =     J     L     k  �  �  E   &     l  '   �  *   �     �  2   �  4         U  �   o                     2     L  &   d  &   �     �  (   �     �     �  -        ;  	   U  
   _  &   j  (   �     �  D   �          !     .     A     T     c     v  =   }  <   �  !   �       	   (     2  6   G  &   ~     �  '   �  (   �  0     *   <  -   g     �     �     �  '   �     �  *        6  ,   D  =   q     �     �  f   �     >      G  0   h  %   �  0   �     �          '      ?     `     x     �     �     �     �  8   �     '     =  0   W  $   �     �  '   �     �  /   �     (  +   B     n     �     �     �     �     �     �  #   �          *     ;  
   P     [  	   {     �     �     �     �     �     �  
   �     �     �           %   !   2      T      c   %   v      �      �      �   $   �      �   !   !     3!  *   N!     y!     �!  
Yes
No - <b><u>Actions</u></b> <b>All fields</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Edit resource preferences</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Opti_ons</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <b>_Support Request</b> <i>Click on the Support Request tab if you need more help !</i> AL_L Action Add _field names Clear the field Close this window Create a new entry Create a new resource De_scription Default _value: Delete this resource Description: Edit / Save this resource Field _Name: Filename: For_m Go to next matched resource Go to previous matched search Hello World! Launch actions about this resource N_ONE N_othing Ne_xt Ne_xt Tip New Next No Not Urgent
Medium
Urgent
Very Urgent Open in Excel
Save as CSV Open the calendar widget Password: Port: Pre_vious Previe_w before print Previe_w in editor Previous Print documents Read my Requests Remove selected entry Remove this entry Repeat latest _action Requests: S_earch: Search ID: Search with this name Send Support Request Send a new request Server: Set to _Default Set to default value Shortcuts Text _and Icons This Page does not exist !
Please retry Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP license Tiny ERP, Field Preference target Titre User _Manual User: Value _Preference View _logs Yes Your selection: _About... _Add _Connect... _Contact _Contextual Help _Delete _Details _Disconnect _Domain: _Execute a plugin _File _Help _Icons only _Menubar _New _Nothing _Open _Options _Plugins _Preferences _Preview in PDF _Print _Read my requests _Remove _Save _Save options _Select _Send a request _Tips _Unselect _User _Waiting Requests _only for you for _all users on _field: test 
Да
Нет - <b><u>Действия</u></b> <b>Все поля</b> <b>Запоните эту форму чтобы отправить ошибку и/или запрос на поддержку..</b>
<i>
Ваш запрос будет отправлен в Tiny ERP мы постараемся ответить как можно скорее.
Но мы не сможем ответить Вам если ваша компания не является партнером Tiny ERP. </i> <b>Редактировать установки объектов</b> <b>Код ошибки:</b> <b>Поля для экспорта</b> <b>Импортировать поля</b> <b>Опции</b> <b>Значение применимо для:</b> <b>Значение применимо если:</b> <b>Поддержка</b> <i>Щелкните по вкладке поддержка, если вам дребуется дополнительная справка !</i> Все Действие Добавить поля Очистить поле Закрыть окно Создать новую запись Создать новый объект Описание Значение по умолчанию Удалить Описание: Редактировать/Сохранить Название поля Файл: Форма К следующему объекту К предыдущему объекту Привет Мир! Запустить действия для этого ресурса Ничего Ничего Следующий Следующая Создать Следующий Нет Не важно
Средне
Важно
Очень важно Открыть в Excel
Сохранить в csv файле Открыть календарь Пароль: Порт: Предыдущий Предв. просмотр перед печатью Просмотр в редакторе Предыдущий Напечатать документы Прочитать мои запросы Удалить выделенную запись Удалить текущую запись Повторить посл. действие Запросы: Пои_ск Искать ID:  Искать с таким именем Отправить запрос Отправить новый запрос Сервер: Установить по умолчанию Установить значение по умолчанию Ярлыки Текст и иконки Запрошенная страница не существует !
Попробуйте еще раз Tiny ERP Tiny ERP - О программе Tiny ERP - Прикрепленные файлы Tiny ERP - Подтверждение Tiny ERP - выбор даты и времени Tiny ERP - выбор даты Tiny ERP - Диалог Tiny ERP - Ошибка Tiny ERP - Экспорт в CSV Tiny ERP - Фильтр Tiny ERP - Формы Tiny ERP - Ссылка Tiny ERP - Лист Tiny ERP - логин Tiny ERP - Установки Tiny ERP - Редактирование объектов Tiny ERP - Поиск Tiny ERP - Search Widgets Tiny ERP - древовидные ресурсы Tiny ERP - редактор форм Tiny ERP лицензия Tiny ERP, Установка полей Титр Руководство пользователя Пользователь: Предустановка значения Посмотреть лог Да Ваш выбор: О программе Добавить _Подключение... _Контакты Контекстная помощь _Удалить Подробно Отключение Домен Выполнить плагин _Файл Помощь Только иконки Меню Создать Ничего Открыть Опции Плагины Установки Просмотр в pdf Печать Прочитать запросы Удалить Сохранить Сохранить установки _Выделить Отправить запрос Tips _Отменить выделение Пользователь Ожидающие запросы только для вас для всех пользователей по полю тест 