��    N     �
              �  �   �  �   �     �     �  8   �  #   �  (     (   >     g     i     z     �     �  
   �  
   �  
   �  
   �  
   �  
   �  
   �  
   �  
     
                  &     -     =  �   C     +  J   A     �     �  '   �  �   �     �  #   �     �  %         .     O     `     s     �     �     �     �     �     �       #   6      Z     {     �     �     �     �     �          $     7     N     `     x  ?   �  t   �  0   H     y     ~     �     �  "   �     �     �  
   �                 (      /      >      E   J   U      �   �   �   w   Y!     �!     �!  	   �!     �!     	"     %"     ."     6"     L"     T"     g"     z"     �"     �"     �"     �"     �"  	   �"  	   �"     �"     	#     #     $#     4#     F#     M#     b#     o#     ~#     �#     �#     �#  
   �#  	   �#     �#     �#     �#     $     $     "$     %$     6$     C$  	   S$     ]$     e$     j$     x$     �$     �$     �$     �$     �$     �$  
   �$     �$     %     %     /%  	   ?%     I%     \%  "   o%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%  	   �%     &     &     &     5&     C&     H&     Q&     f&     r&     u&  $   |&     �&     �&     �&     �&     �&     '     
'      '     0'  	   A'     K'     _'     m'  /   {'     �'  	   �'     �'     �'     �'     �'     �'     (     !(     1(     >(     N(     c(     t(     �(     �(     �(     �(     �(  	   �(     �(     �(     �(     )     )  	   )     $)  	   :)     D)  
   ])     h)     ~)     �)     �)     �)     �)     �)     �)  	   �)     �)     �)     *  (   *     F*     Z*     k*     s*     �*  A   �*     �*     �*  '   +  �   ,+  �   �+     �,     �,     �,     �,      �,     �,     -     -     /-     H-     Z-     k-     ~-     �-     �-     �-     �-     �-     �-     .     !.     ;.     S.     d.     t.     �.     �.  !   �.     �.     �.     �.     �.     �.     �.     /     /  
   /  %   /     D/     \/     i/  j   m/  
   �/     �/  	   �/     �/     0     0     0     (0     90     A0     J0     V0     t0  
   }0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     1     1     1      1     )1     61     F1     M1     _1     g1     y1     1     �1     �1  
   �1  
   �1     �1  	   �1     �1  	   �1     �1     �1     �1     2     2  
   '2     22  n  72  �   �3  �   a4     L5     T5  C   j5  ,   �5  ,   �5  .   6     76     96     J6     Y6     i6  
   y6  
   �6  
   �6  
   �6  
   �6  
   �6  
   �6  
   �6  
   �6  
   �6     �6     �6     �6     �6     7    7     8  G   +8     s8     �8  0   �8    �8  �   �9  %   �:      ;  (   ";      K;     l;     };     �;     �;     �;     �;     �;     �;     <  &   7<  #   ^<  ,   �<     �<     �<     �<      =     =     6=     U=     o=     �=     �=     �=     �=  T   �=  �   G>  .   �>     �>      ?     ?     ?  !   5?     W?     m?  	   }?     �?     �?     �?     �?     �?     �?  a   �?     G@  �   \@  w   �@     ^A     mA     |A     �A     �A     �A     �A     �A     �A     �A     B     B     &B     AB     [B     oB     �B  
   �B  	   �B     �B     �B     �B     �B     �B     �B     C     C      C     2C     :C     XC     nC     C     �C     �C     �C     �C     �C     �C     �C     �C     D     D     ,D     <D     CD     JD     ZD  
   gD  .   rD  +   �D     �D     �D     �D     �D     E     E     3E     BE     OE     cE     wE     �E     �E     �E     �E     �E     �E     �E  
   �E     F     F     F     F     ,F     2F     JF     eF     rF     zF     �F     �F     �F  
   �F  A   �F     G     !G     2G  #   LG     pG     �G     �G     �G     �G     �G     �G     �G     �G  5   H     IH     OH     \H     lH     �H     �H     �H     �H     �H     �H     �H     I     I      8I     YI  *   aI     �I     �I     �I     �I     �I     �I     �I     J     "J     *J     9J     QJ     ^J  
   yJ     �J     �J     �J     �J     �J     �J     �J     K     'K     /K     5K     RK  7   hK     �K     �K     �K     �K     �K  M   L     cL     vL  &   �L  �   �L  �   IM     N     N     &N     ;N     QN     qN     �N     �N      �N     �N     �N     �N     O     O     +O     EO     [O     vO     �O     �O     �O     �O     �O     �O     P     P     /P     @P     ZP     bP     hP     pP  
   �P     �P     �P     �P     �P  (   �P     �P     Q     !Q  u   %Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R     %R  *   .R  	   YR     cR     oR     �R     �R     �R  
   �R  
   �R     �R     �R     �R     �R     �R     
S     S  
    S     +S     2S     CS     OS     ]S     iS  
   �S     �S     �S     �S     �S     �S     �S  	   �S     �S  	   �S     �S      T  
   T     T     2T     AT     YT     oT     xT   
(c) 2003-TODAY - Tiny sprl
Tiny ERP is a product of Tiny sprl:

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgium

Tel : (+32)81.81.37.00
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - GTK Client - v%s

Tiny ERP is an Open Source ERP+CRM
for small to medium businesses.

The whole source code is distributed under
the terms of the GNU Public Licence.

(c) 2003-TODAY, Tiny sprl

More Info on www.TinyERP.com ! 
Yes
No # Employees: (Get Tiny ERP announces, docs and new releases by email) (must not contain any special char) (must not contain any special character) (use 'admin', if you did not changed it) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shist> + <Tab> <Tab> <b>
Write concurrency warning:
</b>
This document has been modified while you were editing it.
  Choose:
   - "Cancel" to cancel saving.
   - "Compare" to see the modified version.
   - "Write anyway" to save your current version.
 <b><u>Actions</u></b> <b>About Tiny ERP</b>
<i>The most advanced Open Source ERP &amp; CRM !</i> <b>All fields</b> <b>Backup a database</b> <b>Change your super admin password</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Complete this form to submit your bug and/or send a support request.</b>
<i>
Your request will be sent to the Tiny ERP team and we will reply shortly.
Note that we may not reply if you do not have a support contract with Tiny or an official partner.</i> <b>Connect to a Tiny ERP server</b> <b>Create a new database</b> <b>Database created successfully!</b> <b>Edit resource preferences</b> <b>Error 404</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Keep Informed</b> <b>Operation in progress</b> <b>Opti_ons</b> <b>Predefined exports</b> <b>Restore a database</b> <b>Shortcuts for TinyERP</b> <b>Shortcuts in relation fields</b> <b>Shortcuts in text entries</b> <b>Support contract id:</b> <b>Tiny ERP Survey</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <b>Your Company:</b> <b>Your Email:</b> <b>Your company:</b> <b>Your email:</b> <b>Your interrest:</b> <b>Your name:</b> <b>_Support Request</b> <b>support contract id</b> <i>Click on the Support Request tab if you need more help !</i> <i>Please fill in the following form in order to help us to improve Tiny ERP and better target new developments.</i> <i>When editing a resource in a popup window</i> AL_L Action Add _field names Add a new line/field Add an attachment to this resource Administrator Password Application Error! Attachment Auto-Complete text field Auto-Detect Bottom CSV Parameters Change Change password Check this box if you want demo data to be installed on your new database. Choose a database... Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database that will be created. The name must not contain any special character. Exemple: 'terp'. Clear the field Client Mode Close Tab Close this window Close window without saving Company: Compare Concurrency exception Connect Copy selected text Copy this resource Country: Create a new database Create a new entry Create a new resource Cut selected text Database creation Database: Databases Date de début De_scription Default Theme Default _value: Default language: Delete Delete this resource Description: Dro_p database E-Mail: Edit / Save this resource Edit this entry Edition Widgets Emergency: Encoding: Error details Explain what you did: Explain your problem: Expor_t data... F1 F2 Field Separater: Field _Name: File to Import: Filename: Filter: Find Find / Search Find a resource For_m Go to next matched resource Go to previous matched search Go to resource ID Hello World Hello World! Horizontal Hour: How did you hear about us: I_mport data... Import from CSV Industry: Keyboard Shortcuts Keyboard shortcuts Launch actions about this resource Left Lines to Skip: Load demonstration data: Main Shortcuts Menu Minute: Mode PDA N_ONE N_othing Ne_xt Ne_xt Tip New New database name: New password confirmation: New password: Next Next Tab Next editable widget Next record No Normal Not Urgent
Medium
Urgent
Very Urgent Old password: Open Source: Open current field Open in Excel
Save as CSV Open the calendar widget Open... Operation in progress Other comments: Others Comments: Password: Paste selected text Phone Number: Phone number: Please wait,
this operation may take a while... Port: Pre_vious Pre_vious Tip Previe_w before print Previe_w in editor Previous Previous Tab Previous editable widget Previous record Previous tab Print documents Protocol connection: Read my Requests Reloa_d / Undo Reload Reload / Undo Form Remove selected entry Remove this entry Repeat latest _action Requests: Restore a database Right Right Toolbar S_earch: Save Save List Save and Close window Save text Search / Open a resource Search ID: Search with this name Send Support Request Send a new request Server Server: Set Default Set to _Default Set to default value Shortcuts State: Super admin password: Support Request Switch current view: form / list / graph Switch to list/form Switch view mode System: Tabs default orientation Tabs default position Tell us why you try Tiny ERP and what are your current softwares: Text Delimiter: Text _and Icons This Page does not exist !
Please retry This is the URL of the Tiny ERP server. Use 'localhost' if the server is installed on this computer. Click on 'Change' to change the address. This is the password of the user that have the rights to administer databases. This is not a Tiny ERP user, just a super administrator. If you did not changed it, the password is 'admin' after installation. Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - License Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Selection Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP Message Tiny ERP Survey Tiny ERP Theme Tiny ERP license Tiny ERP server: Tiny ERP, Field Preference target Tip of the Day Titre Top User _Manual User: Value _Preference Vertical View View _logs We plan to offer services on Tiny ERP We plan to use Tiny ERP Write anyway Yes You can connect to the new database using one of the following account:

    Administrator: admin / admin  Your Role: Your selection: _About... _Add _Backup database _Connect... _Contact _Contextual Help _Delete _Details _Disconnect _Display a new tip next time? _Domain: _Duplicate _Execute a plugin _File _Forms _Go to resource ID... _Help _Icons only _License _Menu _Menubar _New _New Home Tab _New database _Nothing _Open _Options _Plugins _Preferences _Preview in PDF _Print _Read my requests _Remove _Restore database _Save _Save options _Select _Send a request _Shortcuts _Text only _Theme _Tiny ERP _Tips _Unselect _User _Waiting Requests _only for you for _all users http://localhost:8069 on _field: test Project-Id-Version: tinyerp-client
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-12 15:46+0100
PO-Revision-Date: 2007-09-26 14:10+0100
Last-Translator: Dan Horák <dan@danny.cz>
Language-Team: Czech <dan@danny.cz>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Czech
X-Poedit-Country: CZECH REPUBLIC
 
(c) 2003-DNES - Tiny sprl
Tiny ERP je produkt firmy Tiny sprl:

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgie

Tel/Fax: (+32)81.81.37.00
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - GTK Klient - v%s

Tiny ERP je Open Source ERP+CRM
pro malé a střední firmy.

Kompletní zdrojový kód je distribuován podle
pravidel GNU Public Licence.

(c) 2003-DNES, Tiny sprl

Více informací na www.TinyERP.com ! 
Ano
Ne Počet zaměstnanců: (Dostávat oznámení, dokumentaci a nové verze Tiny ERP e-mailem) (nesmí obsahovat žádný speciální znak) (nesmí obsahovat žádný speciální znak) (použijte 'admin', jestli jste ho nezměnili) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shift> + <Tab> <Tab> <b>
Varování při souběhu zápisů:
</b>
Tento dokument byl změněn během provádění vašich úprav:
  Vyberte:
   - "Zrušit" pro zrušení ukládání.
   - "Porovnat" pro zobrazení změněné verze.
   - "Přesto zapsat" pro zapsání vaší verze.
 <b><u>Akce</u></b> <b>O Tiny ERP</b>
<i>Nejpokrokovější Open Source ERP &amp; CRM !</i> <b>Všechna pole</b> <b>Zálohování databáze</b> <b>Změňte své heslo pro správce systému</b> <b>Vyplňte tento formulář pro odeslání chyby a/nebo požadavku na podporu.</b>
<i>
Váš požadavek bude odeslán firmě Tiny ERP, která zanedlouho odpoví.
Pokud váše servisní organizace není partnerem firmy Tiny ERP, odpověd nelze vymáhat.
</i> <b>Vyplňte tento formulář pro odeslání chyby a/nebo požadavku na podporu.</b>
<i>
Váš požadavek bude odeslán firmě Tiny ERP, která zanedlouho odpoví.
Odpověď nemusíte dostat, pokud nemáte smlouvu s firmou TinyERP nebo jejím partnerem</i> <b>Připojení k Tiny ERP serveru</b> <b>Vytvořit novou databázi</b> <b>Databáze úspěšně vytvořena!</b> <b>Upravit nastavení zdroje</b> <b>Chyba 404</b> <b>Chybový kód</b> <b>Pole pro export</b> <b>Pole pro import</b> <b>Být informován</b> <b>Probíhá operace</b> <b>Volby</b> <b>Předdefinované exporty</b> <b>Obnovit databázi</b> <b>Klávesové zkratky pro TinyERP</b> <b>Klávesové zkratky pro pole</b> <b>Klávesové zkratky pro textová pole</b> <b>Id smlouvy o podpoře:</b> <b>Tiny ERP průzkum</b> <b>Hodnota platí pro</b> <b>Hodnota platí pokud</b> <b>Vaše společnost:</b> <b>Vaše emailová adresa:</b> <b>Vaše společnost:</b> <b>Vaše emailová adresa:</b> <b>Váš zájem:</b> <b>Vaše jméno:</b> <b>Požadavek na podporu</b> <b>id smlouvy o podpoře</b> <i>Klikněte na záložku Požadavek na podporu, pokud potřebuje další pomoc!</i> <i>Prosíme o vyplnění následujícího formuláře, které nám pomůže zdokonalovat Tiny ERP a lépe směrovat jeho vývoj.</i> <i>Při editování ve vyskakovacím okně</i> VŠE Akce Přidat jména polí Přidat nový řádek/pole Přidat přílohu k tomuto zdroji Heslo administrátora Chyba aplikace! Příloha Samodokončovací textové pole Automatická detekce Dole Parametry CSV Změnit Změnit heslo Zaškrtněte toto políčko, pokud chcete nainstalovat ukázková data do vaší nové databáze. Vyberte databázi... Vyberte výchozí jazyk, který bude nainstalován do této databáze. Nové jazyky lze přidávat po instalaci z administračního menu. Vyberte název databáze, která bude vytvořena. Název nesmí obsahovat žádný speciální znak. Příklad: 'terp'. Vyčistit pole Režim klienta Zavřít záložku Zavřít toto okno Zavřít okno bez ukládání Společnost: Porovnat Výjimka souběhu Spojit Kopírovat vybraný text Kopírovat tento zdroj Země: vVytvořit novou databázi Vytvořit novou položku: Vložit nový zdroj Vyjmout vybraný text Vytvoření databáze Databáze: Databáze Výchozí datum Popis Implicitní téma Výchozí hodnota: Výchozí jazyk: Smazat Odstranit tento zdroj Popis: Zrušit databázi E-mail: Upravit / Uložit tento zdroj Upravit tuto položku Editovací prvky Nebezpečí: Kódování: Podrobnosti o chybě Vysvětlete, co jste dělali: Popište váš problém: Expor_t dat... F1 F2 Oddělovač polí: Jméno pole: Soubor k importu: Název souboru: Filtr: Najít Najít / Hledat Najít zdroj Formulář Přejít na následující výsledek hledání Přejít na předchozí výsledek hledání Přejít na zdroj s ID Ahoj světe Ahoj světe! Horizontální Hodina: Jak jste se o nás dověděli: I_mport dat... Import z CSV Oblast podnikání: Klávesové zkratky Klávesové zkratky Spustit akce k tomuto zdroji Vlevo Vynechané řádky: Nahrát ukázková data: Hlavní zkratky Menu Minuta: Režim PDA ŽÁDNÝ Nic Da_lší Následující tip Nový Název nové databáze: Potvrzení nového hesla:  Nové heslo: Další Následující karta Další editovatelný prvek Následující záznam Ne Normální Není naléhavé
Středně naléhavé
Naléhavé
Velmi naléhavé Staré heslo:  Otevřít zdroj: Otevřít aktuální pole Otevřít v Excelu
Uložit jako CSV Otevřít kalendář Otevřít... Probíhá operace Další komentáře: Další komentáře: Heslo: Vložit vybraný text Telefonní číslo: Telefonní číslo: Prosím čekejte,
tato operace může trvat dlouho... Port: _Předchozí Předchozí tip Náhled před tiskem Náhled v editoru Předchozí Předchozí karta Předchozí editovatelný prvek Předchozí záznam Předchozí tab Tisknout dokumenty Protokol pro spojení Přečíst mé požadavky Znovu načíst / Přejít _zpět Obnovit Znovu načíst formulář a zrušit změny Odstranit vybranou položku Odstranit tuto položku. Opakovat poslední akci Požadavky: Obnovit databázi Vpravo Pravá nástrojová lišta Hl_edat: Uložit Uložit seznam Uložit a zavřít okno Uložit text Vyhledat / Otevřít zdroj Hledat ID: Hledat s tímto názvem Odeslat požadavek na podporu Odeslat nový požadavek Server Server: Nastavit jako výchozí Vrátit výchozí Nastavit výchozí hodnotu Zkratky Stav: Heslo super administrátora: Požadavek na podporu Přepnout aktuální pohled: formulář / seznam / graf Přepnout na seznam/formulář Přepnout režim zobrazení Systém: Výchozí orientace karet Výchozí pozice karet Řekněte nám proč zkoušíte Tiny ERP a jaký je váš současný systém: Oddělovač textu: Text _a ikony Tato stránka neexistuje!
Zkuste znovu Toto je URL adresa Tiny ERP serveru. Použijte 'localhost' pokud server nainstalován na tomto počítači. Pro změnu adresy klikněte na tlačítko Změna. Toto je heslo uživatele, který bude mít práva spravovat databázi. Nejde o uživatele Tiny ERP, ale o super administrátora. Pokud jste neprovedli změnu, heslo po instalaci je 'admin'. Tiny ERP Tiny ERP - O programu Tiny ERP - Příloha Tiny ERP - Potvrzení Tiny ERP - Výběr data a času Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Chyba Tiny ERP - Export do CSV souboru Tiny ERP - Filtr Tiny ERP - Formuláře Tiny ERP licence Tiny ERP - Odkaz Tiny ERP - Seznam Tiny ERP - Přihlášení Tiny ERP - Nastavení Tiny ERP - Úpravy zdrojů Tiny ERP - Hledání Tiny ERP - Hledání widgetů Tiny ERP - Výběr Tiny ERP - Strom zdrojů Tiny ERP - Formuláře Tiny ERP zpráva Tiny ERP průzkum Téma Tiny ERP Tiny ERP licence Tiny ERP server: Tiny ERP, Nastavení pole Tip dne Titre Nahoře Uživatelský manuál Uživatel: Hodnota nastavení Vertikální Pohled Zobrazit záznamy Plánujeme poskytovat služby s Tiny ERP Plánujeme používat Tiny ERP Přesto zapsat Ano Můžete se připojit k nové databázi pomocí jednoho z následujících účtů:

   Administrátor: admin / admin Vaše pozice: Váš výběr: _O programu... _Přidat Zálohovat databázi Připojit se... _Kontakt Kontextová nápověda Smazat _Detaily _Odpojit Zobrazit tip při příštím spuštění? _Doména: _Duplikovat Spustit zásuvný modul _Soubor _Formuláře _Přejít na zdroj s ID... Nápověda Jen _ikony _Licence _Menu Hlavní nabídka _Nový _Nová domácí karta _Nová databáze _Nic _Otevřít _Volby Zásuvné moduly Nastav_ení Náhled v PDF Vy_tisknout Přečíst mé požadavky _Odstranit Obnovit databázi _Uložit Uložit nastavení _Vybrat Odeslat požadavek _Zkratky Jen _text _Téma _Tiny ERP Tipy Zrušit výběr _Uživatel Čekající požadavky pouze pro vás pro všechny uživatele http://localhost:8069 na pole: Test 