��    I     d
              �  �   �  �   f     S     [  8   h  #   �  (   �  (   �               *     9     I  
   Y  
   d  
   o  
   z  
   �  
   �  
   �  
   �  
   �  
   �     �     �     �     �     �  �   �     �  J   �     <     N  '   g  �   �     v  #   w     �  %   �      �     �          #     ;     S     p     �     �     �  #   �      �          2     I     f     �     �     �     �     �     �     �       ?   .  t   n  0   �                     1  "   F     i     �  
   �     �     �     �     �     �     �  J   �     ;   �   P   w   �      l!     |!  	   �!     �!     �!     �!     �!     �!     �!     �!     "     "     "     4"     G"     ]"     o"  	   �"  	   �"     �"     �"     �"     �"     �"     �"     �"     �"     
#     #     !#     ;#     K#  
   [#  	   f#     p#     ~#     �#     �#     �#     �#     �#     �#     �#  	   �#     �#      $     $     $     #$     )$     E$     c$     u$     �$  
   �$     �$     �$     �$     �$  	   �$     �$     �$  "   
%     -%     2%     A%     Z%     i%     n%     v%     %     �%     �%  	   �%     �%     �%     �%     �%     �%     �%     �%     &     &     &  $   &     <&     J&     W&     j&     �&     �&     �&     �&     �&  	   �&     �&     �&     '  /   '     F'  	   L'     V'     d'     z'     �'     �'     �'     �'     �'     �'     �'     �'     (     (     4(     F(  	   \(     f(     y(     (     �(     �(  	   �(     �(  	   �(     �(  
   �(     �(     �(     )     ')     .)     6)     B)     R)  	   g)     q)     x)     �)     �)     �)     �)     �)     �)  A   �)     <*     L*  '   \*  �   �*  �   +     �+     �+     �+     ,      ),     J,     d,     v,     �,     �,     �,     �,     �,     �,     �,     -     -     8-     J-     d-     y-     �-     �-     �-     �-     �-     �-  !   �-     .     ..     4.     8.     E.     K.     ].  
   f.  %   q.     �.     �.     �.  j   �.  
   +/     6/  	   F/     P/     U/     f/     r/     {/     �/     �/     �/     �/     �/  
   �/     �/     �/     �/     �/     0     0     "0     +0     10     :0     ?0     M0     [0     d0     j0     s0     |0     �0     �0     �0     �0     �0     �0     �0     �0     �0  
   �0  
   1     1  	   1     1  	   %1     /1     51     G1     U1     d1  
   z1     �1  D  �1  �   �2    �3     �4     �4  A   �4  )   �4  )   5  +   D5     p5     r5     �5     �5     �5  
   �5  
   �5  
   �5  
   �5  
   �5  
   �5  
   �5  
   �5  
   
6  
   6      6     (6     /6     66     F6    L6     R7  n   i7     �7     �7  /   8    =8    U9  #   m:  !   �:  1   �:     �:     �:     ;     ";     :;  %   X;     ~;     �;     �;     �;  "   �;  "   <     )<     9<  !   Q<  !   s<     �<     �<     �<     �<     �<     
=  %   #=      I=  =   j=  �   �=  0   *>     [>     d>     m>     ~>     �>     �>     �>     �>     �>     ?     ?     ?     '?     .?  D   <?     �?  �   �?  y   &@     �@     �@     �@     �@     �@     �@  	   �@      A     &A     /A     GA     WA     ]A  
   xA     �A     �A     �A     �A     �A     �A     �A     �A     B     B     -B     3B     BB     HB     \B     dB     }B     �B  
   �B  
   �B     �B     �B     �B     �B     	C     C     C     C     +C     CC     PC     WC     ^C     oC  
   �C     �C     �C     �C     �C     �C  
   �C     �C      D     (D     :D     GD     ^D     qD     �D     �D     �D     �D     �D     �D     �D     �D     �D     �D  	   �D     E     E     !E     3E     IE  	   VE     `E     tE     �E     �E     �E  4   �E     �E     �E     F  *   F     ?F  
   ]F     hF     �F     �F     �F     �F     �F     �F  +   �F     G     G     G     5G     OG  	   dG     nG     �G     �G     �G     �G     �G     �G     �G     H  
   (H     3H     QH     ^H     tH     }H     �H     �H     �H     �H     �H     �H  
   �H     �H     I     I     -I     4I  
   <I     GI     \I     wI     I     �I     �I     �I     �I     �I     �I     �I  U   J     ZJ     nJ  1   |J  �   �J  �   <K     L     L     /L     FL     _L     ~L     �L     �L     �L     �L     �L     �L     M     M     .M     EM     \M     vM  !   �M     �M     �M     �M     N     N     *N     9N     KN  "   [N     ~N     �N     �N     �N     �N     �N     �N     �N  )   �N     O     9O     FO  k   JO     �O     �O     �O     �O     �O     P     P     P     -P     4P  
   @P  =   KP     �P     �P     �P     �P     �P     �P     �P     �P  	   �P     �P     Q     Q     Q     7Q     IQ     NQ     WQ     ]Q     fQ     sQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R  	   R      R     ,R     5R     BR     [R     jR     }R     �R     �R   
(c) 2003-TODAY - Tiny sprl
Tiny ERP is a product of Tiny sprl:

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgium

Tel : (+32)81.81.37.00
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - GTK Client - v%s

Tiny ERP is an Open Source ERP+CRM
for small to medium businesses.

The whole source code is distributed under
the terms of the GNU Public Licence.

(c) 2003-TODAY, Tiny sprl

More Info on www.TinyERP.com ! 
Yes
No # Employees: (Get Tiny ERP announces, docs and new releases by email) (must not contain any special char) (must not contain any special character) (use 'admin', if you did not changed it) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shist> + <Tab> <Tab> <b>
Write concurrency warning:
</b>
This document has been modified while you were editing it.
  Choose:
   - "Cancel" to cancel saving.
   - "Compare" to see the modified version.
   - "Write anyway" to save your current version.
 <b><u>Actions</u></b> <b>About Tiny ERP</b>
<i>The most advanced Open Source ERP &amp; CRM !</i> <b>All fields</b> <b>Backup a database</b> <b>Change your super admin password</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Complete this form to submit your bug and/or send a support request.</b>
<i>
Your request will be sent to the Tiny ERP team and we will reply shortly.
Note that we may not reply if you do not have a support contract with Tiny or an official partner.</i> <b>Connect to a Tiny ERP server</b> <b>Create a new database</b> <b>Database created successfully!</b> <b>Edit resource preferences</b> <b>Error 404</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Operation in progress</b> <b>Opti_ons</b> <b>Predefined exports</b> <b>Restore a database</b> <b>Shortcuts for TinyERP</b> <b>Shortcuts in relation fields</b> <b>Shortcuts in text entries</b> <b>Support contract id:</b> <b>Tiny ERP Survey</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <b>Your Company:</b> <b>Your Email:</b> <b>Your company:</b> <b>Your email:</b> <b>Your interrest:</b> <b>Your name:</b> <b>_Support Request</b> <b>support contract id</b> <i>Click on the Support Request tab if you need more help !</i> <i>Please fill in the following form in order to help us to improve Tiny ERP and better target new developments.</i> <i>When editing a resource in a popup window</i> AL_L Action Add _field names Add a new line/field Add an attachment to this resource Administrator Password Application Error! Attachment Auto-Complete text field Auto-Detect Bottom CSV Parameters Change Change password Check this box if you want demo data to be installed on your new database. Choose a database... Choose the default language that will be installed for this database. You will be able to install new languages after installation through the administration menu. Choose the name of the database that will be created. The name must not contain any special character. Exemple: 'terp'. Clear the field Client Mode Close Tab Close this window Close window without saving Company: Compare Concurrency exception Connect Copy selected text Copy this resource Country: Create a new database Create a new entry Create a new resource Cut selected text Database creation Database: Databases Date de début De_scription Default Theme Default _value: Default language: Delete Delete this resource Description: Dro_p database E-Mail: Edit / Save this resource Edit this entry Edition Widgets Emergency: Encoding: Error details Explain what you did: Explain your problem: Expor_t data... F1 F2 Field Separater: Field _Name: File to Import: Filename: Filter: Find Find / Search Find a resource For_m Go to next matched resource Go to previous matched search Go to resource ID Hello World Hello World! Horizontal Hour: How did you hear about us: I_mport data... Import from CSV Industry: Keyboard Shortcuts Keyboard shortcuts Launch actions about this resource Left Lines to Skip: Load demonstration data: Main Shortcuts Menu Minute: Mode PDA N_ONE N_othing Ne_xt Ne_xt Tip New New database name: New password confirmation: New password: Next Next Tab Next editable widget Next record No Normal Not Urgent
Medium
Urgent
Very Urgent Old password: Open Source: Open current field Open in Excel
Save as CSV Open the calendar widget Open... Operation in progress Other comments: Others Comments: Password: Paste selected text Phone Number: Phone number: Please wait,
this operation may take a while... Port: Pre_vious Pre_vious Tip Previe_w before print Previe_w in editor Previous Previous Tab Previous editable widget Previous record Previous tab Print documents Protocol connection: Read my Requests Reloa_d / Undo Remove selected entry Remove this entry Repeat latest _action Requests: Restore a database Right Right Toolbar S_earch: Save Save List Save and Close window Save text Search / Open a resource Search ID: Search with this name Send Support Request Send a new request Server Server: Set Default Set to _Default Set to default value Shortcuts State: Super admin password: Support Request Switch to list/form Switch view mode System: Tabs default orientation Tabs default position Tell us why you try Tiny ERP and what are your current softwares: Text Delimiter: Text _and Icons This Page does not exist !
Please retry This is the URL of the Tiny ERP server. Use 'localhost' if the server is installed on this computer. Click on 'Change' to change the address. This is the password of the user that have the rights to administer databases. This is not a Tiny ERP user, just a super administrator. If you did not changed it, the password is 'admin' after installation. Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - License Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Selection Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP Message Tiny ERP Survey Tiny ERP Theme Tiny ERP license Tiny ERP server: Tiny ERP, Field Preference target Tip of the Day Titre Top User _Manual User: Value _Preference Vertical View _logs We plan to offer services on Tiny ERP We plan to use Tiny ERP Write anyway Yes You can connect to the new database using one of the following account:

    Administrator: admin / admin  Your Role: Your selection: _About... _Add _Backup database _Connect... _Contact _Contextual Help _Delete _Details _Disconnect _Display a new tip next time? _Domain: _Duplicate _Execute a plugin _File _Forms _Go to resource ID... _Help _Icons only _License _Menu _Menubar _New _New Home Tab _New database _Nothing _Open _Options _Plugins _Preferences _Preview in PDF _Print _Read my requests _Remove _Restore database _Save _Save options _Select _Send a request _Shortcuts _Text only _Theme _Tiny ERP _Tips _Unselect _User _Waiting Requests _only for you for _all users http://localhost:8069 on _field: test Project-Id-Version: Tiny ERP Client 4.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-12 15:46+0100
PO-Revision-Date: 2006-12-05 16:22+0100
Last-Translator: Jacek Kałucki <laborm@rz.onet.pl>
Language-Team: PL <laborm@rz.onet.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
(c) 2003-TODAY - Tiny sprl
Tiny ERP jest produktem Tiny sprl:

Tiny sprl
40 Chaussée de Namur
1367 Gérompont
Belgium

Tel/Fax: +32.10.68.94.39
Mail: sales@tiny.be
Web: http://tiny.be 
Tiny ERP - GTK Client - v%s

Tiny ERP jest Otwartym Oprogramowaniem ERP+CRM
dla małych i średnich przedsiębiorstw.

Cały kod źródłowy jest dostępny
na warunkach Publicznej Licencji GNU.

(c) 2003-TODAY, Tiny sprl

Więcej informacji na www.TinyERP.org ! 
Tak
Nie Liczba pracowników: (Chcę otrzymywać informacje z Tiny ERP pocztą elektronniczną) (nie może zawierać znaków specjalnych) (nie może zawierać znaków specjalnych) (wpisz 'admin', jeśli go nie zmieniałeś) - <Ctrl> + <Enter> <Ctrl> + <Esc> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + C <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + V <Ctrl> + W <Ctrl> + X <Enter> <PgDn> <PgUp> <Shift> + <Tab> <Tab> <b>
Ostrzeżenie o nadpisaniu danych:
</b>
Ten dokument został zmodyfikowany, podczas gdy Ty dokonywałeś w nim zmian.
  Wybierz:
   - "Anuluj" by anulować zapis.
   - "Sprawdź" by zobaczyć dokonane zmiany.
   - "Wymuś zapis" aby zapisać Twoją wersję.
 <b><u>Operacje</u></b> <b>O Tiny ERP</b>
<i>Jeden z najbardziej zaawansowanych projektów Otwartego Oprogramowania ERP &amp; CRM!</i> <b>Wszystkie pola</b> <b>Archiwizuj bazę danych</b> <b>Zmień hasło administratora bazy danych</b> <b>Wypełnij tę ankietę aby przekazać informację o błędzie lub zapytanie.</b>
<i>
Będzie ono przekazane do Tiny ERP, byś wkrótce mógł otrzymać odpowiedź.
Jeśli twój opiekun nie jest partnerem Tiny ERP, zastrzegamy sobie możliwość nie udzielenia odpowiedzi.
</i> <b>Wypełnij tę ankietę aby przekazać informację o błędzie lub zapytanie.</b>
<i>
Będzie ono przekazane do Tiny ERP, byś wkrótce mógł otrzymać odpowiedź.
Jeśli twój opiekun nie jest partnerem Tiny ERP, zastrzegamy sobie możliwość nie udzielenia odpowiedzi.
</i> <b>Połącz z serwerem Tiny ERP</b> <b>Utwórz nową bazę danych</b> <b>Baza danych została pomyślnie utworzona!</b> <b>Edytuj pozycję</b> <b>Błąd 404</b> <b>Kod błędu:</b> <b>Pola do eksportu</b> <b>Pola do zaimportowania</b> <b>Procedura w trakcie realizacji</b> <b>_Opcje</b> <b>Predefiniowane eksporty</b> <b>Odtwórz bazę danych</b> <b>Skróty dla TinyERP</b> <b>Skróty w polach powiązań</b> <b>Skróty w polach tekstowych</b> <b>Kontakt:</b> <b>Ankieta Tiny ERP</b> <b>Wartość odpowiednia dla:</b> <b>Wartość odpowiednia dla:</b> <b>Firma:</b> <b>Twój adres email: </b> <b>Twoja firma:</b> <b>Twój adres email:</b> <b>Jestem zainteresowany:</b> <b>Imię i nazwisko:</b> <b>Zgłoszenie pomocy technicznej</b> <b>identyfikator zgłoszenia</b> <i>Kliknij na menu 'Wsparcie' jeśli potrzebujesz pomocy!</i> <i>Wypełnij proszę poniższą ankietę, aby pomóc nam udoskonalić Tiny ERP i lepiej przygotować kolejne wersje programu.</i> <i>Podczas edycji pozycji w oknie dialogowym</i> Wszystko Operacja Dodaj nazwy pól Dodaj nowy wiersz/pole Dodaj załącznik do pozycji _Hasło administratora Błąd programu! Załącznik Automatyczne uzupełnianie pól Autodetekcja Dół Parametry CSV Zmień Zmień hasło Zaznacz jeśli chcesz załadować do bazy danych, dane przykładowe. Wybierz bazę danych... Wybierz domyślny język, jaki będzie używany w tej bazie danych. Nowe języki mogą zostać dodane po instalacji z panelu administratora. Wybierz nazwę dla nowej bazy danych. Nazwa nie może zawierać żadnych znaków specjalnych. Przykładowa nazwa: 'terp'. Wyczyść pole Tryb klienta Zamknij zakładkę Zamknij okno Zamknij bez zapisywania Firma: Porównaj Wyjątek równoczesnego dostępu Połącz Kopiuj zaznaczony tekst Kopiuj pozycję Kraj: Utwórz nową bazę danych Dodaj wpis Dodaj nową pozycję Wytnij zaznaczony tekst Tworzenie bazy danych Baza danych: _Bazy danych Data początkowa Opis Motyw domyślny Wartość domyślna: Domyślny język: Usuń Usuń pozycję Opis: _Usuń bazę danych E-Mail: Edytuj / Zapisz pozycję Edytuj wpis Kontrolki edycyjne Priorytet: Kodowanie: Szczegóły błędu Opisz swoje postępowanie: Szczegóły problemu: Ekpor_tuj dane... F1 F2 Separator pól: Nazwa pola: Plik do zaimportowania: Nazwa pliku: Filtr: Szukaj Znajdź / Szukaj Znajdź pozycję For_mularz Następna pozycja z listy Poprzednia pozycja z listy Idż do ID pozycji Witaj świecie! Witaj świecie! W poziomie Godzina: Skąd dowiedziałeś się o nas: I_mportuj dane... Importuj CSV Rodzaj działalności: Skróty klawiszowe Skróty klawiszowe Operacje na pozycji Na lewo Pomiń wiersze: Załaduj dane przykładowe: Główne skróty Menu Minuta: Tryb PDA Nic Brak Do przodu Następna wskazówka Dodaj Nowa baza danych: Potwierdzenie hasła: Nowe hasło: Następny Następna zakładka Następna kontrolka Następna pozycja Nie Normalny Niski priorytet
Normaly priorytet
Pilne
Bardzo pilne Poprzednie hasło: Otwarte Oprogramowanie: Otwórz pole Otwórz arkuszkalkulacyjny
Zapisz jako CSV Otwórz kontrolkę kalendarza Otwórz... Procedura w trakcie realizacji Dodatkowe informacje: Dodatkowe uwagi: Hasło: Wklej zaznaczony tekst Numer telefonu: Telefon: Proszę czekać,
operacja chwilę potrwa... Port: W tył Poprzednia wskazówka Podgląd przed _wydrukiem Podgląd _w edytorze Poprzedni Poprzednia zakładka Poprzednia kontrolka Poprzednia pozycja Poprzednia zakładka Drukuj dokument Protokół sieciowy: Pokaż moje zgłoszenia O_dświerz / Cofnij Usuń zaznaczone wpisy Usuń wpis Powtórz ost_atnią operację Zgłoszenia: Odtwórz bazę danych Na prawo Prawy pasek narzędziwy _Szukaj: Zapisz Zapisz listę Zapisz i zamknij Zapisz tekst Szukaj / Pokaż pozycję Szukaj ID: Szukaj tego tekstu Wyślij zgłoszenie Nowe zgłoszenie Serwer Serwer: Domyślnie Ustaw jako domyślne Ustaw wartość domyślną Skróty Status: Hasło administratora: Wsparcie Zmień widok Lista/Formularz Zmień widok System: Orientacja zakładek Położenie zakładek Dlaczego zainteresowałeś się Tiny ERP i jakiego oprogramowania aktualnie używasz: Ogranicznik tekstu: Tekst i ikony Strona nie istnieje!
Proszę spróbować ponownie To jest adres URL serwere Tiny ERP. Wpisz 'localhost' jeśli serwer zainstalowany jest na tym komputerze. Kliknij na 'Zmień' by go zmienić. Jest to hasło użytkownika mającego uprawnienia do zarządzania bazą danych. Nie jest to użytkownik Tiny ERP, lecz jego administrator. Jeśli go nie zmieniałeś, domyślnie po instalacji brzmi ono 'admin'. Tiny ERP Tiny ERP - O programie Tiny ERP - Załącznik Tiny ERP - Potwierdzenie Tine ERP - Wybór daty i czasu Tiny ERP - Wybór daty Tiny ERP - Dialog Tiny ERP - Błąd Tiny ERP - Eksport CSV Tiny ERP - Filtr Tiny ERP - Formularze Tine ERP - Licencja Tiny ERP - Skrót Tiny ERP - Lista Tiny ERP - Autoryzacja Tiny ERP - Preferencje Tiny ERP - Edycja pozycji TIny ERP - Szukaj Tiny ERP - Kontrolki wyszukiwania Tiny ERP - Wybór operacji Tiny ERP - Drzewo elementów Tiny ERP - kontrolka formularza Wiadomośc od Tiny ERP Ankieta Tiny ERP Motyw Tiny ERP Tiny ERP licencja Serwer Tiny ERP Tiny ERP, Parametr pola docelowego Wskazówka dnia Jako Góra Instrukcja użytkownika Użytkownik: Wartość parametru W pionie Pokaż rejestr zdarzeń Planujemy oferować wsparcie dla Tiny ERP Planujemy używać Tiny ERP Wymuś zapis Tak Nie można się połączyć z nowa bazą danych z następującego konta:

    Administrator: admin / admin  Twoje stanowisko: Twój wybór: O progr_amie... Dodaj Archiwizuj _bazę danych Połą_cz... Kontakt Pomo_c kontekstowa _Usuń Szczegóły _Rozłącz Zaznacz jeśli chcesz otrzymywać wskazówki w przyszłości? _Domena: Powiel pozycję Uruchom wtyczkę _Plik _Formularze Przejdź do pozycji o ID... Pomo_c Tylko _ikony _Licencja _Menu Pasek narzędziowy _Nowy _Nowa zakładka startowa _Nowa baza danych Brak _Otwórz Widok _Wtyczki _Preferencje Podgląd _PDF Wydruki _Pokaż moje zgłoszenia Usuń Odtwó_rz bazę danych Zapi_sz Zapamiętaj u_stawienia _Zaznacz _Zgłoś problem _Skróty Tylko _tekst Mo_tyw _Tiny ERP Podpowiedzi _Odznacz _Użytkownik _Zgłoszenia oczekujące dotyczy Ciebie dotyczy wszystkich http://localhost:8069 w _polu: test 