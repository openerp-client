��    K      t             �     �     �     �  �   �     �     �          (     E  ?   a     �     �     �     �     �     �     �            	   6     @     \     z  "   �     �     �     �  $   �     �  	   �     �               (     >  	   P     Z     _     u     �     �     �     �  	   �  '   �     �     	     	     ,	     D	     ^	     p	     �	     �	     �	     �	     �	     �	     �	     
     
     1
     K
     e
     }
  !   �
     �
     �
     �
     �
     �
     �
     �
     �
                	     !  �   9     *     C     \     u     �  N   �     �               )     A     X     a     w     �     �  #   �     �     �  $   �       	        )  $   ,     Q     o     |     �     �      �     �     �     �     �          *  	   E     O     i     �  -   �     �     �     �     �          "     6     G     a     s     �     �     �     �     �     �          %     C     `  
   r     }     �     �     �     �     �     �     �  
Yes
No - <b><u>Actions</u></b> <b>All fields</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <i>Click on the Support Request tab if you need more help !</i> Action Clear the field Close this window Create a new entry Create a new resource Delete Delete this resource Description: Edit / Save this resource Filename: Go to next matched resource Go to previous matched search Hello World! Launch actions about this resource New Next No Not Urgent
Medium
Urgent
Very Urgent Open the calendar widget Password: Previous Print documents Read my Requests Remove selected entry Remove this entry Requests: Save Search with this name Send Support Request Send a new request Server: Set to _Default Set to default value Shortcuts This Page does not exist !
Please retry Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP license Tiny ERP, Field Preference target Tip of the Day User: Value _Preference Yes Your selection: _File _Options _User 
Si
No - <b><u>Acciones</u></b>  <b>Todos los campos</b> <b>Complete esta forma para enviar un bug y/o una solicitud de soporte.</b>
<i>
Su solicitud será enviada a Tiny ERP y será contestada a la brevedad.
Nota, es posible que no obtenga respuesta si su empresa no es un socio de Tiny ERP.
</i> <b>Código de Error:</b> <b>Campos a exportar</b> <b>Campos a importar</b> <b>Valor aplicable para:</b> <b>Valor aplicable si:</b> <i>Click en la pestaña de Solicitud de soporte si requiere de más ayuda!</i> Acción Limpiar el campo Cerrar esta ventana Crear una nueva entrada Crear un nuevo recurso Eliminar Eliminar este recurso Descripción Editar / Guardar este recurso Archivo: Ir al siguiente recurso coincidente Ir a la búsqueda previa Hola Mundo! Ejecutar acciones sobre este recurso Nuevo Siguiente No No urgente
Media
Urgente
Muy Urgente Abrir el widget de calendario Contraseña: Previo Imprimir documentos Leer mis Solicitudes Eliminar la entrada seleccionada Eliminar esta entrada Solicitudes Guardar Buscar con este nombre Enviar Solicitud de Soporte Enviar una nueva solicitud Servidor: Ajustar a _Predeterminado Ajustar el valor predeterminado Atajos Esta página no existe !
Por favor introduzca Tiny ERP Tiny ERP - Acerca Tiny ERP - Anexo Tiny ERP - Confirmación Tiny ERP - Selección de Fecha Tiny ERP - Diálogo Tiny ERP - Error Tiny ERP - Exportar a CSV Tiny ERP - Filtro Tiny ERP - Formas Tiny EPR - Liga Tiny ERP - Lista Tiny ERP - Entrada Tiny ERP - Preferencias Tiny ERP - Edición de Recurso Tiny ERP - Búsqueda Tiny ERP - Widgets de Búsqueda Tiny ERP - Árbol de Recursos Tiny ERP - widgets de formas Tiny ERP licencia Tiny ERP,  Tip del día Usuario: Valor _Preferencia Si Su selección _Archivo Opciones _Usuario 