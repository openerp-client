��    �                    �  �   �     �     �     �     �            
   "  
   -  
   8  
   C  
   N  
   Y  
   d     o     v     }  J   �     �  �   �      �     �          #     ;     P     `     z      �     �     �     �          $     9     L     ^  ?   v  t   �  0   +     \     a     h  "   y     �     �     �     �  	   �     �     �               0     C     L     _     u     �     �     �     �     �     �     �     �     �       	        &     <     L     ]  	   j     t     y     �     �     �     �     �     �     �  	             ,  "   ?     b     q     �     �     �     �  	   �     �     �     �     �     �     �     �  $   �     
     $     =  	   M     W     k     y  	        �     �     �     �     �     �     �     �               -     <     R     d  	   z     �     �  	   �     �     �  
   �     �     �                    (     8  	   M     W     g     {     �     �     �  '   �     �     �     �           $     E     _     q     �     �     �     �     �     �     �                2     L     d     t     �  !   �     �     �     �     �     �  
   �  %   �     !     9  
   =     H  	   X     b     g     s     |     �     �     �     �     �     �     �     �     �     �     �          
               "     /     ?     F     X     `     f     t     |  
   �     �  	   �     �  	   �     �     �     �     �  
   �     �  L  �  �   J     8      A      Y      [      l      ~   
   �   
   �   
   �   
   �   
   �   
   �   
   �      �   	   �      �   N   !     W!  �   j!  +   5"     a"     v"     �"     �"     �"     �"  $   #  ,   '#     T#     r#     �#     �#     �#     �#     �#     $  R   $  }   k$  A   �$     +%     1%     8%  &   O%  &   v%     �%     �%     �%     �%     �%  #   �%  	   &     "&     >&     W&     ]&     t&     �&     �&     �&     �&     �&     �&     �&     �&     '     '     ''  
   :'     E'     c'     y'  
   �'  
   �'     �'     �'     �'  	   �'     �'     �'     (     (     ,(     C(     V(     c(  '   s(     �(     �(  	   �(  
   �(     �(     �(     �(     )     
)     )  &   $)     K)     _)     d)  ,   k)  #   �)     �)     �)  	   �)     �)     *     *  
   %*     0*     A*     a*  	   {*     �*  $   �*     �*     �*     �*     �*     
+     $+     C+     U+     p+     y+  	   �+  	   �+      �+     �+  
   �+     �+     �+     ,     ",     *,     I,     h,     �,     �,     �,     �,     �,     �,     �,  ;   -     @-     I-     Z-     l-  $   �-     �-     �-     �-     �-     �-     .     #.     ;.     L.     a.     }.     �.     �.     �.     �.     �.     �.     /     +/     :/     @/  	   W/     a/     g/  4   y/  (   �/     �/     �/  
   �/     �/     0     0     %0     20     N0     \0     e0  	   y0     �0     �0     �0     �0     �0     �0     �0     �0     �0  	   �0     1     
1      1     21     ?1  
   U1  
   `1     k1     1     �1  	   �1     �1  	   �1     �1     �1  	   �1     �1     �1     2  	   2     $2   
Tiny ERP - GTK Client - v%s

Tiny ERP is an Open Source ERP+CRM
for small to medium businesses.

The whole source code is distributed under
the terms of the GNU Public Licence.

(c) 2003-TODAY, Tiny sprl

More Info on www.TinyERP.com ! 
Yes
No # Employees: - <Ctrl> + <Enter> <Ctrl> + <PgDn> <Ctrl> + <PgUp> <Ctrl> + D <Ctrl> + F <Ctrl> + L <Ctrl> + N <Ctrl> + O <Ctrl> + S <Ctrl> + W <PgDn> <PgUp> <b><u>Actions</u></b> <b>About Tiny ERP</b>
<i>The most advanced Open Source ERP &amp; CRM !</i> <b>All fields</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Edit resource preferences</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Keep Informed</b> <b>Opti_ons</b> <b>Predefined exports</b> <b>Shortcuts for TinyERP</b> <b>Shortcuts in text entries</b> <b>Support contract id:</b> <b>Tiny ERP Survey</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <b>Your Company:</b> <b>Your email:</b> <b>Your name:</b> <b>_Support Request</b> <i>Click on the Support Request tab if you need more help !</i> <i>Please fill in the following form in order to help us to improve Tiny ERP and better target new developments.</i> <i>When editing a resource in a popup window</i> AL_L Action Add _field names Add an attachment to this resource Auto-Complete text field Auto-Detect Clear the field Client Mode Close Tab Close this window Close window without saving Connect Copy selected text Copy this resource Country: Create a new entry Create a new resource Cut selected text Date de début De_scription Default Theme Default _value: Delete Delete this resource Description: E-Mail: Edit / Save this resource Edit this entry Encoding: Explain your problem: Expor_t data... Field Separater: Field _Name: Filename: Find Find / Search Find a resource For_m Go to next matched resource Go to previous matched search Hello World! How did you hear about us: I_mport data... Industry: Keyboard Shortcuts Keyboard shortcuts Launch actions about this resource Lines to Skip: Main Shortcuts Mode PDA N_ONE N_othing Ne_xt Ne_xt Tip New Next Next Tab Next editable widget Next record No Normal Not Urgent
Medium
Urgent
Very Urgent Open in Excel
Save as CSV Open the calendar widget Other comments: Password: Paste selected text Phone number: Port: Pre_vious Pre_vious Tip Previe_w before print Previe_w in editor Previous Previous Tab Previous editable widget Previous record Previous tab Print documents Read my Requests Reloa_d / Undo Remove selected entry Remove this entry Repeat latest _action Requests: S_earch: Save Save List Save and Close window Search / Open a resource Search ID: Search with this name Send Support Request Send a new request Server: Set Default Set to _Default Set to default value Shortcuts Support Request Switch to list/form Switch view mode System: Text Delimiter: Text _and Icons This Page does not exist !
Please retry Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP Survey Tiny ERP Theme Tiny ERP license Tiny ERP, Field Preference target Tip of the Day Titre User _Manual User: Value _Preference View _logs We plan to offer services on Tiny ERP We plan to use Tiny ERP Yes Your Role: Your selection: _About... _Add _Connect... _Contact _Contextual Help _Delete _Details _Disconnect _Domain: _Execute a plugin _File _Go to resource ID... _Help _Icons only _Menubar _New _Nothing _Open _Options _Plugins _Preferences _Preview in PDF _Print _Read my requests _Remove _Save _Save options _Select _Send a request _Text only _Theme _Tiny ERP _Tips _Unselect _User _Waiting Requests _only for you for _all users on _field: test Project-Id-Version: Tiny ERP Client 3.5
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-12 15:46+0100
PO-Revision-Date: 2006-12-05 16:22+0100
Last-Translator: Michael Bunk <mb@computer-leipzig.com>
Language-Team: DE <i18n-info@tinyerp.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Tiny ERP - GTK Client - v%s

Tiny ERP ist ein quelloffenes ERP+CRM
für kleine und mittelständische Firmen.

Der gesamte Quellcode steht unter der
GNU Public Licence.

(c) 2003-TODAY, Tiny sprl

Mehr Informationen auf www.TinyERP.org ! 
Ja
Nein Anzahl der Mitarbeiter: - <Strg> + <Enter> <Strg> + <BildAb> <Strg> + <BildAuf> <Strg> + D <Strg> + F <Strg> + L <Strg> + N <Strg> + O <Strg> + S <Strg> + W <BildAb> <BildAuf> <b><u>Aktionen</u></b> <b>Über Tiny ERP</b>
<i>Das fortschrittlichste Open-Source ERP &amp; CRM!</i> <b>Alle Felder</b> <b>Bitte füllen Sie dieses Fourmular aus, </b>
<i>
Ihre Anfrage wird an Tiny ERP geschickt und in Kürze beantwortet.
Falls Service-Anbieter keine Tiny ERP Partner ist, bekommen sie keine Antwort.
</i> <b>Resource voreinstellungen bearbeiten</b> <b>Fehlernummer:</b> <b>zu exportierende Felder</b> <b>zu importierende Felder</b> <b>Bleiben sie informiert</b> <b>Opti_onen</b> <b>zu exportierende Felder</b> <b>Tastaturbefehle für Tiny ERP</b> <b>Tastaturbefehle in Texteingabefeldern</b> <b>Supportvertragsnummer:</b> <b>Tiny ERP Umfrage</b> <b>Wert anwendbar für:</b> <b>Wert anwendbar wenn:</b> <b>Ihre Firma:</b> <b>Ihre Email-Adresse:</b> <b>Ihr Name:</b> <b>_Supportanfrage</b> <i>Klicken Sie auch den 'Support Anfrage' Tab, wenn Sie mehr Hilfe benötigen!</i> <i>Bitte füllen sie das folgende Formular aus, um uns zu helfen Tiny ERP zu verbessern und gezielter weiterzuentwickeln.</i> <i>Wenn sie einen Datensatz in einem Popup-Fenster bearbeiten</i> A_lle Aktion _Feldnamen hinzufügen Anhang zu dieser Ressource hinzufügen Textfeld automatisch vervollständigen Automatische Erkennung Feld löschen Client-Modus Reiter schließen Fenster schliessen Schließe Fenster ohne zu Speichern Verbinden Ausgewählten Text kopieren Diese Ressource kopieren Land: Neuen Eintrag erzeugen Neue Resource erzeugen Ausgewählten Text ausschneiden Einfügedatum Be_schreibung Standardthema _Vorgabewert: Löschen Resource löschen Beschreibung: E-Mail: Resource speichern Eintrag bearbeiten Kodierung: Beschreiben Sie Ihr Problem:  Daten expor_tieren... Feldtrenner: Feld_name: Dateiname: Finden Finden / Suchen Suchen sie eine Ressource For_mular Zum nächsten Treffer Zum letzten Treffer Hallo Welt! Wie haben sie von uns gehört: Daten i_mportieren ... Industrie/Branche: Abkürzungen Tastaturbefehle Aktionen für diese Resource ausführen Zu überspringende Zeilen: Abkürzungen PDA-Modus Keine (_O) Nichts (_O) Nächste (_x) Nächster Tipp (_x) Neu Nächste Nächster Reiter Nächstes bearbeitbares Eingabeelement Nächster Datensatz Nein Normal Nicht dringend
Mittel
Dringend
Sehr dringend Mit Excel öffnen
Als CSV speichern Kalender-Auswahl öffnen Weitere Kommentare: Kennwort: Ausgewählten Text einfügen Telefonnummer: Port: _Vorherige _Vorheriger Tipp Voransicht vor dem Drucken (_W) Voransicht im Editor (_w) Vorherige Vorheriger Reiter Voriges bearbeitbares Eingabeelement Vorheriger Datensatz Vorheriger Reiter Dokumente drucken Meine Anfragen lesen Neu la_den / Rückgängig Ausgewählten Eintrag löschen Eintrag entfernen Letzte _Aktion wiederholen Anfrage: _Suche: Speichern Save List Speichern und Fenster schliessen Ressource suchen / öffnen ID Suchen: Suche mir diesem Namen Support Anfrage abschicken Neue Anfrage senden Server: Auf _Vorgabewert zurücksetzen Auf _Vorgabewert zurücksetzen Auf Vorgabewert zurücksetzen Abkürzungen Supportanfrage Ansicht wechseln Liste/Fomular Ansichtmodus umschalten System: Texttrenner: Text und Symbole (_a) Diese Seite existiert nicht!
Bitte versuchen Sie es nochmal Tiny ERP Tiny ERP - Über Tiny ERP - Anhang Tiny ERP - Bestätigung Tine ERP - Datum und Zeit auswählen Tiny ERP - Datumsauswahl Tiny ERP - Dialog Tiny ERP - Fehler Tiny ERP - CSV-Export Tiny ERP - Filter Tiny ERP - Formulare Tiny ERP - Verknüpfung Tiny ERP - Liste Tiny ERP - Anmeldung Tiny ERP - Voreinstellungen Tiny ERP - Resource bearbeiten TIny ERP - Suche Tiny ERP - Baum Resourcen Tiny ERP - Formularfeld Tiny ERP Umfrage Tiny ERP Thema Tiny ERP Lizenz Tiny ERP - Feld Vorzugsziel Tipp des Tages Titel Benutzeranleitung (_M) Benutzer: _Wert _Logs anzeigen... Wir planen, Dienstleitungen mit Tiny ERP anzubieten. Wir planen Tiny ERP bei uns einzusetzen. Ja Ihre Position: Ihre Wahl: Über... (_A) Hinzufügen (_A) Verbinden... (_C) Kontakt (_C) Kontexthilfe (Website) (_C) Löschen (_D) _Details Verbin_dung trennen _Domäne: Plugin ausführ_en... _Datei Zu Resource ID _gehen... _Hilfe nur Symbole (_I) _Menüleiste _Neu Kei_ne Öffnen (_O) _Optionen _Plugins Einstellungen... (_P) _PDF-Vorschau ... Drucken (_P) Meine Anf_ragen lesen Entfe_rnen _Speichern Optionen _speichern Au_swählen Anfrage _senden nur _Text _Thema _Tiny ERP Tipp des Tages... Abwählen (_U) _Benutzer _Wartende Anfragen nur für Sie (_o) für _alle Benutzer in _Feld: test 