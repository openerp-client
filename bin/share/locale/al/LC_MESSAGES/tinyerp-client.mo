��    S      �             L     T     V     l  �   ~      e     �     �     �     �     �  ?        B     I     Y     k     ~     �     �     �     �     �  	   �     �          *  "   7     Z     ^     c  $   f     �     �  	   �     �     �     �     �     �     	  	    	     *	  
   /	     :	     P	     e	     x	     �	     �	  	   �	  '   �	     �	     �	     �	     
      
     @
     Z
     l
     }
     �
     �
     �
     �
     �
     �
               -     G     a     y  !   �     �     �     �     �     �     �     �     �     �                         3  �   G      /     P     e     �     �     �  @   �                .     B     Y     m     |     �     �     �     �  )   �  '   �       !   .     P     U     \  )   _     �     �     �     �     �     �     �     �       	        )  	   /     9     L     f     z     �     �     �  -   �     �     �             %   6     \     x     �     �     �     �     �     �               +     G     X     r  !   �     �  *   �     �     �                    #     6  	   >     H  
   Z  
Yes
No - <b><u>Actions</u></b> <b>All fields</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Edit resource preferences</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <i>Click on the Support Request tab if you need more help !</i> Action Clear the field Close this window Create a new entry Create a new resource Date de début Delete Delete this resource Description: Edit / Save this resource Filename: Go to next matched resource Go to previous matched search Hello World! Launch actions about this resource New Next No Not Urgent
Medium
Urgent
Very Urgent Open in Excel
Save as CSV Open the calendar widget Password: Port: Previous Print documents Read my Requests Remove selected entry Remove this entry Requests: Save Search ID: Search with this name Send Support Request Send a new request Server: Set to _Default Set to default value Shortcuts This Page does not exist !
Please retry Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP license Tiny ERP, Field Preference target Tip of the Day Titre User: Value _Preference Yes Your selection: _File _Options _Preview in PDF _User 
Po!
Jo - <b><u>Akcionet</u></b> <b>Krejt fushat</b> <b>Kompletoni kete formular dhe/ose kerko perkrahjen.</b>
<i>
Kjo kerkes do ti dergohet Tiny ERP dhe te merrni pergjigjen shume shpejt.
Verejtje ne nuk do te ju pergjigjemi nese kompania e juaj nuk eshte fare Tiny ERP partner.
</i> <b>Azhuro resurs preferencat</b> <b>Kodi gabimit:</b> <b>Fushat per eksportim</b> <b>Fushat per import</b> <b>Vlera aplikueshme per:</b> <b>Vlera Aplikueshme nese:</b> <i>Kliko ne Tabin e kerkesave per ndihme nese ju nevojitet !</i> Akcioni Pastro fushen Mbylle kete dritare Krijo te dhenat e reja Krijo resursin e ri Date de début Fshije Fshije kete resurs Pershkrimi: Azhuro / Ruaje kete resurs Emri fajllit: Shko te resursi i ardheshem qe pershtatet Shko te kerkesat e gjetura paraprakisht Tungit o njerzi! Lanso akcionet e ketyre resurseve E Re Tjetri Jo Jo urgjent
Mesatare
Urgjent
Shume urgjent Hape ne Excel
Ruaje si CSV Hape kalendarin Shifra: Porti: Paraprak Shtypi dokumentet Lexoj kerkesat e mia Largo shenimin e zgjedhur Largo kete shenim Kerkesat: Ruaje Kerko ID: Kerko me kete emer Dergo Kerkesen Perkrahjes Dergo kerkesen e re Serveri: Vendosi _Definuarat Vendose vleren standarde RrugeShkurtesa Kjo faqe nuk ekziston!
Ju lutem tentoni prape Tiny ERP Tiny ERP - Rreth Tiny ERP - Bashkangjitja Tiny ERP - Konfirmimi Tiny ERP - Zgjedhja e Dates dhe Kohes Tiny ERP - Zgjedhja e dates Tiny ERP - Dialogu Tiny ERP - Gabimi Tiny ERP - Eksporto ne CSV Tiny ERP - Filtrat Tiny ERP - Formularet Tiny ERP - Linqet Tiny ERP - Lista Tiny ERP - Qasja Tiny ERP - Preferenca Tiny ERP - Resurs Azhuruesi Tiny ERP - Kerko Tiny ERP - Kerko elementi Tiny ERP - Pema e Resurseve Tiny ERP - elementet e formularit Tiny ERP licensa Tiny ERP, Shenjestra e fushes se preferuar Tipet e dites Titujt Perdoruesi: _Preference Vlera Po! Zgjedhjet e juaja: _Fajlli _Opcionet _Parashiko ne PDF Perdoruesi 