��    R      �              <     =     E     [  �   m      T     u     �     �     �     �  ?   �     1     8     H     Z     m     �     �     �     �     �  	   �     �     �       "   &     I     M     R  $   U     z     �  	   �     �     �     �     �     �     �  	   	     	  
   	     )	     ?	     T	     g	     o	     	  	   �	  '   �	     �	     �	     �	      
     &
     @
     R
     c
     |
     �
     �
     �
     �
     �
     �
               -     G     _  !   p     �     �     �     �     �     �     �     �     �     �  D  �  	   =     G     ]  �   t  &   r     �     �     �     �       L   !     n     u     �     �     �     �     �     �     �     �       %   #  #   I  	   m  #   w     �     �     �  7   �     �               !     (     1     E     W     s     �     �     �     �     �     �  	   �     �     
     )  9   1     k     |     �  #   �     �     �     �          (     :     R     b     s     �  #   �     �     �      �          /  -   @     n     z  	   �     �     �     �     �  	   �     �  	   �   
Yes
No <b><u>Actions</u></b> <b>All fields</b> <b>Complete this form to submit your bug and/or request a support.</b>
<i>
Your request will be send to Tiny ERP and we will reply you shortly.
Note that we may not reply you if your Service Company is not a Tiny ERP partner.
</i> <b>Edit resource preferences</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Value applicable for:</b> <b>Value applicable if:</b> <i>Click on the Support Request tab if you need more help !</i> Action Clear the field Close this window Create a new entry Create a new resource Date de début Delete Delete this resource Description: Edit / Save this resource Filename: Go to next matched resource Go to previous matched search Hello World! Launch actions about this resource New Next No Not Urgent
Medium
Urgent
Very Urgent Open in Excel
Save as CSV Open the calendar widget Password: Port: Previous Print documents Read my Requests Remove selected entry Remove this entry Requests: Save Search ID: Search with this name Send Support Request Send a new request Server: Set to _Default Set to default value Shortcuts This Page does not exist !
Please retry Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date & Time selection Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP license Tiny ERP, Field Preference target Tip of the Day Titre User: Value _Preference Yes Your selection: _File _Options _Preview in PDF _User Project-Id-Version: pt_BR
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-12 15:46+0100
PO-Revision-Date: 2005-07-08 13:08-0300
Last-Translator: Cosme Correa <cosme@pcs.com.br>
Language-Team:  <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.1
 
Sim
Não <b><u>Ações</u></b> <b>Todos os campos</b> <b>Complete este formulário para enviar seu bug e/ou solicitar nosso suporte</b> <i>Sua solicitação será enviada para tiny ERP e nós iremos lhe responder rapidamente Note que nós não responderemos se soa companhia não é Parceiro da Tiny ERP</i> <b>Editar preferências de recurso</b> <b> Erro de código</b> <b>Arquivos para exportar</b> <b>Campos a Importar<b> <b>Valor aplicável para:</b> <b>Valor aplicável se:</b> <i>Clique na área de pedido de suporte se você Precisar de mais ajuda!</i> Ação Limpar o campo Fechar esta janela Criar nova entrada Criar novo recurso Data de início Apagar Apagar este recurso Descrição: Editar / Salvar este recurso Nome do arquivo: Ir até o Próximo recurso encontrado Ir  até a última busca encontrada Oi Mundo! Carregar ações sobre este recurso Novo Próximo Não Não Urgente<br> Médio<br>  Urgente<br> Muito Urgente. Abrir em excel
 Salvar como CSV Abrir o calendário Senha: Porta: Anterior Imprimir documentos Leia meus pedidos Remover entrada selecionada Remover esta entrada Pedidos: Salvar Procurar ID Procurar com este nome  Enviar Pedido de Suporte Enviar novo pedido Servidor: Selecionar para Padrão Selecionado para valor padrão Atalhos Esta página não existe!<br> Por favor tente novamente.  Tiny ERP - Sobre Tiny ERP - Anexo Tiny ERP - Confirmação Tiny ERP - Seleção de Dia e Hora  Tiny ERP - Seleção de data Tiny ERP - Diálogo Tiny ERP - Erro Tiny ERP - Exportar para CSV Tiny ERP - Filtro Tiny ERP - Formulários Tiny ERP - Link Tiny ERP - Lista Tiny ERP - Entrar Tiny ERP - Preferências Tiny ERP - Editar Recurso Tiny ERP  Tiny ERP - Procurar Tiny ERP- Mecanismos de Procura Tiny ERP - Recursos de Estrutura Tiny ERP - Formulários Tiny ERP licence Tiny ERP , Campo Preferência Tiny ERP - alvo Dica do Dia Titlulo Usuário: Valor_Preferência Sim Sua seleção _Arquivo _Opções _Preview em PDF _Usuário 