��    B      ,              <     =     E     G     ]     o     �     �     �  ?   �               &     8     K     a     h     }     �  	   �     �  "   �     �     �     �     �  	                  &     <  	   N     X     ]     p     x     �  	   �  '   �     �     �     �     �          1     C     T     m          �     �     �     �     �     �               8     P     a     p     v     z     �     �     �  G  �  	   �	     �	     �	     
      
     1
     N
     k
  Q   �
     �
     �
     �
     �
          1     :  	   I     S  
   o     z  )   �     �     �     �     �     �     �     �  "        1  	   F     P     X     n     w  &   �     �  +   �     �     �          #     <     \     v     �     �     �     �     �     �     
     $     @  #   T     x  !   �     �     �     �     �     �     �     �        
Yes
No - <b><u>Actions</u></b> <b>All fields</b> <b>Error code:</b> <b>Fields to export</b> <b>Fields to import</b> <b>Value applicable for:</b> <i>Click on the Support Request tab if you need more help !</i> Action Clear the field Close this window Create a new entry Create a new resource Delete Delete this resource Description: Edit / Save this resource Filename: Hello World! Launch actions about this resource New Next No Open the calendar widget Password: Previous Print documents Remove selected entry Remove this entry Requests: Save Send a new request Server: Set to _Default Set to default value Shortcuts This Page does not exist !
Please retry Tiny ERP Tiny ERP - About Tiny ERP - Attachment Tiny ERP - Confirmation Tiny ERP - Date selection Tiny ERP - Dialog Tiny ERP - Error Tiny ERP - Export to CSV Tiny ERP - Filter Tiny ERP - Forms Tiny ERP - Link Tiny ERP - List Tiny ERP - Login Tiny ERP - Preferences Tiny ERP - Ressource Edit Tiny ERP - Search Tiny ERP - Search Widgets Tiny ERP - Tree Resources Tiny ERP - forms widget Tiny ERP license Tip of the Day User: Yes Your selection: _File _Options _User Project-Id-Version: Tiny ERP V2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-12-12 15:46+0100
PO-Revision-Date: 2005-04-17 20:27+0200
Last-Translator: Szilard Novaki <novaki@agmen-software.com>
Language-Team: Hungarian <hu@gnome.hu>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Igen
Nem - <b><u>Műveletek</u></b> <b>Minden mező</b> <b>Hibakód:</b> <b>Exportálandó mezők</b> <b>Importálandó mezők</b> <b>Alkalmazandó:</b> <i>Kattintson a Támogatás kérése fülre ha segítséget szeretne kérni !</i> Művelet Mező törlése Ablak bezárása Új bejegyzés létrehozása Új elem létrehozása Törlés Elem törlése Leírás: Elem szerkesztése/mentése Fájlnév: Helló világ! Elemmel kapcsolatos műveletek indítása Új Következő Nem Naptár felület megnyitása Jelszó: Előző Dokumentumok nyomtatása Kiválasztott bejegyzés törlése Bejegyzés törlése Kérések Mentés Új kérés küldése Szerver: _Alapértelmezett Alapértelmezett értékek betöltése Billentyűparancsok Ez az oldal nem létezik !
Próbálja újra Tiny ERP Tiny ERP - Névjegy Tiny ERP - Melléklet Tiny ERP - Jóváhagyás Tiny ERP - Dátum kiválasztás Tiny ERP - Dialógusablak Tiny ERP - Hiba Tiny REP - Exportálás CSV-be Tiny ERP - Szűrés Tiny ERP - Űrlapok Tiny ERP - Hivatkozás Tiny ERP - Lista Tiny ERP - Belépés Tiny ERP - Beállítások Tiny ERP - Elem szerkesztő Tiny ERP - Keresés Tiny ERP - Felületelemek keresése Tiny ERP - Fa elemek Tiny ERP - űrlap kezelőfelület Tiny ERP licensz A nap tippje Felhasználó: Igen Az ön választásai: _Fájl _Opciók _Felhasználó 